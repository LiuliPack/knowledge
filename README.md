翻新中，部分信息未及时更新或被临时移除。

# 文件清单

文件路径及文件 | 描述
| - | -
[AwesomeHentai.md](AwesomeHentai.md) | 去除知沟效应，打破"文化"屏障 —— 里·二次元索引。
[FontList.md](FontList.md) | 降低字型使用门槛 —— 中文开源字体清单。
[HACGWallpaperSet.json](HACGWallpaperSet.json) | 琉璃神社壁纸包相关。
[HanaCreamQuotes.json](HanaCreamQuotes.json) | 记录極彩花夢字幕组组长的“发电”言论 —— HanaCream 语录。
[HardcoreDegree.md](HardcoreDegree.md) | 里番重口度和评价。
[VidFootage/*](VidFootage/) | 贯彻利他心 —— 视频创作素材。

# 关于我

## 我是谁
始于2019年4月4日得心理性性欲亢奋患者。LiuliPack(LP, 琉璃壁纸包)这个名字是因为想溯源所有琉璃神社壁纸包出处。不过因为 Mikusa(https://img.himiku.com) 也在做相似的事，就一度停摆。

**贯彻人生观**
- 始终相信利他心。 Always believe in altruism.
- 不玩零和博弈，不搞你输我赢。 No zero-sum game, no you lose I win. 
- 人类是整体，不要搞对立。 Humanity is community, don't get antagonistic.
- 不藏私，除非会直接或间接暴露我身份的。 No hiding, unless it would reveal my identity.
- 前面的区域以后再来探索吧。 To be explored.

## 我在哪

- 常用
  - [琉璃神社](https://www.hacg.mom/wp/participant/lpofficial) - [社区](https://www.hacg.mom/wp/bbs)
  - [灵梦广场](https://acg.is/u/LiuliPack) - [求助板块](https://acg.is/t/wanted)
  - [Gmail](mailto:liulipack@gmail.com)
  - [GreasyFork](https://greasyfork.org) - [讨论(新建请求)](https://greasyfork.org/zh-CN/discussions/requests?read=unread)
- 低频
  - [哔哩哔哩](https://space.bilibili.com/2091925785)
  - [YouTube](https://www.youtube.com/channel/UCFZdUtZ7p4OZLW-HkBF1oXA)
  - [Bilibili](https://www.bilibili.tv/en/space/1436316599)
  - [Nyaa-sukebei](https://sukebei.nyaa.si/user/liulipack)
- 占位
  - [萌幻之乡](https://www.hmoe11.net/author/732546) - 鹰小队之前有在这投过稿。
  - [绅士仓库](https://cangku.io/user/292049/post) - 之前神社有人问能不能访问。
  - [bangumi新账号](https://bgm.tv/user/631093) - 使用 AniDB 代替。
  - [bangumi-废弃](https://bgm.tv/user/544460) - 忘记之前有注册过。尝试和站长联系把号合并，但并未理睬。
  - [Facebook](https://www.facebook.com/profile.php?id=100069540391908) - 弃用。后续内容在 Twitter 更新。
  - [南+](https://south-plus.net/u.php?uid-1419056.html) - [询问&求物板块](https://south-plus.net/thread.php?fid=48#c) - 弃用。桜都不更里番，待着也没意义。
  - [GitHub](https://github.com/liulipack) - 受软件自由保护协会《[Give Up GitHub: The Time Has Come!(放弃 GitHub：时机以至！)](https://sfconservancy.org/blog/2022/jun/30/give-up-github-launch/)》一文建议，迁移至此处。
  - [Twitter](https://twitter.com/liulipack) - 被 [Youtube Community](https://www.youtube.com/@LiuliPack/community)和[哔哩哔哩动态](https://space.bilibili.com/2091925785/dynamic)替代。

## 我在干什么

### 未来
- [ ] 搞钱。

### 现在
- 我在[魔穗字幕组](https://sukebei.nyaa.si/user/Maho-subs)和[極彩花夢字幕组](https://kyokusai.com)无偿制作动画时间轴和一般压制；~~YuiStar 字幕组时间轴和一般压制。腾讯号被封了，回去的话不知道还能不能认我是组员...；~~
- 新人视频作者；
- 我在琉璃神社和灵梦广场提供无偿帮助；
- 番茄工作法践行者。

### 过去

#### 2022年
<!-- **月日 [~]** 暂别。假如再也见不到你，就再祝你下午好，晚上好，晚安！(in case i don't see you,good afternoon,good evening,and good night!) -->
**09月13日 [~]** 离开膺小队翻译组。[聊天记录](https://lpack.oss-cn-hangzhou.aliyuncs.com/DOC/2022/09/11/ChatExport.7z)  
**08月03日 [+]** 为 [@DeXiao](https://github.com/De-Xiao) 完成《[契约之吻](https://engage-kiss.com/bddvd/)》S01E04 的部分待校对字幕。   
**07月20日 [+]** 加入[極彩花夢](https://kyokusai.com)。根据 Nyaa 相关资源页信息加入了聊天群组，看到组长抱怨。在私聊表明可以无偿帮忙被拒后，成为组员；  
**05月20日 [+]** 加入魔穗字幕组。此前就想加入魔穗，在御所正好看到[招募信息](https://blog.reimu.net/archives/74531)就入组了。

#### 2021年
**全年总结 [~]** 通过 qBittorrent 统计分享率达 5.67；5月3日至12月31日共完成 28 部动画待校对字幕；在琉璃神社社区、灵梦广场和南+求助板块完成 1660(琉璃神社社区 1599 次，灵梦广场 22 次，南+求助板块 39 次) 次回复，其中灵梦广场和南+经过详细对比完全为帮扶性回复，琉璃神社社区包含部分聊天类回复；完成 3 期《CSGO 高光时刻》系列作品，搬运 3 部他人作品至国际互联网平台并署名；完成 H 萌娘的2021年4月至10月里番合集页面编辑。  
**11月25日 [+]** 通过[幻城字幕组动态](https://t.bilibili.com/596955790783806461)，成功加入 YuiStar 字幕组。  

#### 2020年
**10月06日 [+]** 加入鹰小队字幕组。当时桜都爆出被抄袭事件，不名真相的我选择加入鹰小队。如今看来似乎是选错边了。入组后也没好意思问组长抄袭事件具体什么情况。另外，由于腾讯号被封的原因，导致具体时间不可考。标注时间仅为推测。

# 附录 / Appendix

## 约定式提交 1.0.3-本地化 / conventional commits 1.0.3-localization

本仓库基于[约定式提交 1.0.0](https://www.conventionalcommits.org/zh-hans/v1.0.0/约定式提交规范)的本地化版本。

```
[提交类型](涉及代码1, 涉及代码2): (更新描述1)；(更新描述2)

[必填] (可选)
```

提交类型 | 描述
-- | -
`调` | 调整内容。
`撤` | 撤销提交。

## JSON 文件相关信息

### HanaCream 语录(HanaCreamQuotes.json)

**JSON 模板**

```
[
    {
        "id": "",
        "content": "",
        "source": {
            "have": true,
            "content": "",
            "author": ""
        },
        "link": ""
    }
]
```

**键值描述**

键(Key) | 描述 | 值示例
| - | - | -
`id` | 编号。按照`[日期(YYYYMMDD)]_[序列号]`格式填写。 | `20220406_001`
`content` | 语录内容。通常需要补全句尾符号和换行符。 | `没有涩涩的涩涩，不是我想要的涩涩。`
`source.have` | 是否有原句。此处为布尔值。 | `true`
`source.content` | 原句内容。`source.have`为`false`时移除此键值。 | `没有未来的未来，不是我想要的未来！`
`source.author` | 原句作者。`source.have`为`false`时移除此键值。 | `不详`
`link` | 语录出处。 | `https://t.me/KyokuSai/7643`


### 琉璃神社壁纸包相关(HACGWallpaperSet.json)

**JSON 模板**

```
[
    {
        "id": "",
        "link": "",
        "magnet": "",
        "torrent": "",
        "source": [
            {
                "id": "",
                "title": {
                    "origin": "",
                    "zh_Hans": "",
                    "zh_Hant": "",
                    "ja_JP": "",
                    "ru_RU": "",
                    "en_US": ""
                },
                "author": "",
                "link": [
                    {
                        "site": "",
                        "works": "",
                        "author": ""
                    },
                    {
                        "site": "",
                        "works": "",
                        "author": ""
                    }
                ],
                "license": {
                    "simple": "",
                    "origin": ""
                },
                "rating": "",
                "accessibility": ""
            }
        ]
    }
]
```

**键值描述**

键(Key) | 描述 | 示例
| - | - | -
`id` | 刊号。按照`[年份(YYYY)]-[月份]`格式填写。 | `2023-01`
`link` | 发布链接。 | `https://www.hacg.me/wp/95074.html`
`magnet` | 磁链。 | `magnet:?xt=urn:btih:45639f1dff396034e572f119ec49ceceda380e9b&...`
`torrent` | 磁链文件链接。 | ``
`source.id` | 文件编号。 | `001`
`source.title.origin` | 原始标题。 | ``
`source.title.zh_Hans` | 中文_简体译制或原始标题。 | ``
`source.title.zh_Hant` | 中文_繁体译制或原始标题。 | ``
`source.title.ja_JP` | 日文_日本译制或原始标题。 | ``
`source.title.ru_RU` | 俄文_俄罗斯译制或原始标题。 | ``
`source.title.en_US` | 英文_美国译制或原始标题。 | ``
`source.author` | 绘图作者昵称。 | ``
`source.link.site` | 绘图站点名称。 | ``
`source.link.works` | 绘图链接。 | ``
`source.license.simple` | 绘图精简版权信息。建议使用值：`放弃版权`、`署名使用`、`授权后使用`、`禁止商用`、`禁止二次传播`和`未描述` | `未描述`
`source.license.origin` | 绘图原始版权信息。 | ``
`source.rating` | 绘图内容评级。建议使用值：`0`即老少皆宜地内容、`1`即包含不适合公开浏览地内容，如角色穿着暴露或半暴露、描绘色情内容、`2`即包含令人不适地内容，如肢体离断、身体扩张等 | `0`
`source.accessibility` | 无障碍替代文本。 | ``
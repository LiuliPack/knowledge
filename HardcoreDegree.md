里番重口度和评价。  
![头图/header](assets/Images/HardcoreDegree.png)

# 类型描述

类型 | 描述
| - | -
Ⅰ | 仅包含常规得性交画面。如阴道交、肛交、口交、乳交、手交、扶她等。
Ⅱ | 包含非常规得性交画面。如虫交、兽交、触手、机械奸、异种奸等。
Ⅲ | 包含可能令人不适得性交画面。如脐交、眼交、脑交、肢体离断、身体扩张等。


# 清单

此部分内容更新于：2023-01-26

<!--贩售日(Date) | \[作品发行]作品名(\[production]Title) | 类型(Class) | 短评(Eval)-->
贩售日 | [作品发行]作品名 | 类型 | 短评
-- | - | - | -
20230224 | [[魔人]露出系魔法女子大生クリスティア　第二話　感じちゃって魔力増幅！？　魔法女子大生のエッチな戦い](http://www.a1c.jp/~majin/product/crystear_02.html) | 待分类 | -
20230224 | [[ピンクパイナップル]魔女は結局その客と。。。THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2381) | 待分类 | -
20230224 | [[PoRO]ツグナヒ　褐色ビチギャル・茗子〜鬼棒の轍♥〜](http://www.poro.cc/product/tsugunai_03/petit_tsugunai_03_point.html) | 待分类 | -
20230224 | [[nür]そしてわたしはセンセイに…… 〜めげないブルマ♥〜](http://nur.a1c.jp/nur_brand/product/soshisen/index_soshisen_03.html) | 待分类 | -
20230217 | [[Queen Bee]アオハルスナッチ 1［夏庵］](https://www.mediabank.co.jp/product.php?model=QNB-M110) | 待分类 | -
20230203 | [[メリー・ジェーン]<span title="いもうと">妹</span>はGALかわいい 第1話 属性違いの妹](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20230203 | [[ばにぃうぉ～か～]OVA僕にセフレが出来た理由 #6](https://www.lune-soft.jp/ova/25283) | 待分类 | -
20230203 | [[ばにぃうぉ～か～]OVA僕にセフレが出来た理由 #5](https://www.lune-soft.jp/ova/25261) | 待分类 | -
20230127 | [[nür]ヤリ上がり 〜スレ違いのイジワル♥〜](http://nur.a1c.jp/nur_brand/product/yariagari/index_yariagari_01.html) | 待分类 | -
20230127 | [[ショーテン]メスダチ The Animation 千紗編](https://showten.info/products/anime/stap024.html) | 待分类 | -
20230127 | [[Queen Bee]ハーレム・カルト 3 side HAREM［宇場義行］](https://www.mediabank.co.jp/product.php?model=QNB-M111) | 待分类 | -
20230127 | [[ピンクパイナップル]スリーピース THE ANIMATION](https://www.pinkpineapple.co.jp/detail.php?did=2378) | 待分类 | -
20230127 | [[魔人]おとぎばなしの鬼ごっこ　第一話　赤ずきんとケダモノたち](https://www.a1c.jp/~majin/product/otogi_01.html) | 待分类 | -
20230127 | [[魔人]<span title="援助">○○</span>交配　第八話　夜遊びざかりの不良な彼女は竜の長](https://www.a1c.jp/~majin/product/kouhai_08.html) | 待分类 | -
\- | 上方作品暂未贩售，有可能跳票 | 待分类 | -
20230101 | [[ばにぃうぉ～か～]OVAまこちゃん開発日記 #4](https://www.lune-soft.jp/ova/25156) | 待分类 | -
20230101 | [[ばにぃうぉ～か～]OVAまこちゃん開発日記 #3](https://www.lune-soft.jp/ova/25134) | 待分类 | -
20221227 | [[ピンクパイナップル]異世界来たのでスケベスキルで全力謳歌しようと思う THE ANIMATION 第2巻](https://www.pinkpineapple.co.jp/detail.php?did=2376) | 待分类 | -
20221227 | [[ピンクパイナップル]異世界来たのでスケベスキルで全力謳歌しようと思う THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2374) | 待分类 | -
20230127 | [[ピンクパイナップル]リアルエロゲシチュエーション！2 THE ANIMATION ゴールドディスク](https://www.pinkpineapple.co.jp/detail.php?did=2380) | 待分类 | -
20221223 | [[ショーテン]雷光神姫アイギスマギア ―PANDRA saga 3rd ignition― The Animation 下巻](https://showten.info/products/anime/stap022.html) | 待分类 | -
20221223 | [[nür]ママ喝っ 〜熟れ羞じらうゴゴママ♥〜](http://nur.a1c.jp/nur_brand/product/mamakatu/index_mamakatu_01.html) | 待分类 | -
20221223 | [[PoRO]エロリーマン2　エロ輩姉妹・真冬&愛菜〜卑少女たちの羞恥♥〜](http://www.poro.cc/product/eroriman2_02/petit_eroriman2_02_point.html) | 待分类 | -
20221223 | [[魔人]露出系魔法女子大生クリスティア　第一話　変身魔法は公開露出！？　魔法女子大生クリスティア爆誕！！](https://www.a1c.jp/~majin/product/crystear_01.html) | 待分类 | -
20221202 | [[ばにぃうぉ～か～]OVA僕にセフレが出来た理由 #4](https://www.lune-soft.jp/ova/25072) | 待分类 | -
20221202 | [[ばにぃうぉ～か～]OVA僕にセフレが出来た理由 #3](https://www.lune-soft.jp/ova/25070) | 待分类 | -
20221125 | [[魔人]錬精術士コレットのＨな搾精物語　第二話　褐色踊り子アーシャ参戦！　エッチな誘惑でオーガを討て！](https://www.a1c.jp/~majin/product/collet_02.html) | 待分类 | -
20221125 | [[PoRO]懲らしめ2〜狂育的デパガ指導〜　義妹デパガ・穂波〜魅惑の漏らし男根♥〜](http://www.poro.cc/product/korasime2_02/petit_korasime2_02_point.html) | 待分类 | -
20221125 | [[ショーテン]メスダチ The Animation サツキ編](http://showten.info/products/anime/stap023.html) | 待分类 | -
20221125 | [[ピンクパイナップル]エロゲで全ては解決できる！ THE ANIMATION 第2巻](https://www.pinkpineapple.co.jp/detail.php?did=2372) | 待分类 | -
20221125 | [[魔人]<span title="援助">○○</span>交配　第七話　水着の一日恋人な彼女はエルフの護衛騎士](https://www.a1c.jp/~majin/product/kouhai_07.html) | 待分类 | -
20221118 | [[メリー・ジェーン]僕は小さな淫魔のしもべ 第2話クロエのお食事タイム](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20221118 | [[メリー・ジェーン]Abandon－１００ヌキしないと 出られない不思議な教室－ 第2話ゲームの勝者](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20221104 | [[ばにぃうぉ～か～]OVAサキュバス喚んだら義母が来た！？ #2](https://www.lune-soft.jp/ova/24997) | 待分类 | -
20221104 | [[ばにぃうぉ～か～]OVAサキュバス喚んだら義母が来た！？ #1](https://www.lune-soft.jp/ova/24975) | 待分类 | -
20221028 | [[ショーテン]雷光神姫アイギスマギア ―PANDRA saga 3rd ignition― The Animation 上巻](https://showten.info/products/anime/stap021.html) | 待分类 | -
20221028 | [[nür]ハヂ＋ 〜剥かれる破廉チ♥〜](http://nur.a1c.jp/nur_brand/product/hadiplus/index_hadiplus_02.html) | 待分类 | -
20221028 | [[PoRO]エロリーマン2　憧憬クルエロ・真冬〜見せつける醜恥芯♥〜](http://www.poro.cc/product/eroriman2_01/petit_eroriman2_01_point.html) | 待分类 | -
20221028 | [[Collaboration Works]きつね娘のエッチなお宿 第二話　団体様もいらっしゃい！　女将のアソコは満員御礼♥](https://www.a1c.jp/~collabo/product/kitunemusume_02.html) | 待分类 | -
20221007 | [[メリー・ジェーン]思春期のお勉強 第3話 キスをしてみたいお年頃](https://mary-jane.biz/mysite1/shouhinlist.html) | Ⅰ | -
20221007 | [[WHITE BEAR]夏休み明けの彼女は… 後編](https://www.mediabank.co.jp/product.php?model=WBR-113) | 待分类 | -
20221007 | [[ばにぃうぉ～か～]OVA 僕にセフレが出来た理由 ＃2](https://www.lune-soft.jp/ova/24926) | 待分类 | -
20221007 | [[ばにぃうぉ～か～]OVA 僕にセフレが出来た理由 ＃1](https://www.lune-soft.jp/ova/24907) | 待分类 | -
20220930 | [[PoRO petit]懲らしめ2～狂育的デパガ指導～　蔑みデパガ・彩子〜ガラス張りの棒刺♥〜](http://www.poro.cc/product/korasime2_01/petit_korasime2_01_point.html) | 待分类 | -
20220930 | [[ピンクパイナップル]デリバリーち<span title="待补">◯</span>こを頼みたいお姉さん](https://www.pinkpineapple.co.jp/detail.php?cid=4&did=2362) | 待分类 | -
20220930 | [[ピンクパイナップル]キミはやさしく寝取られる THE ANIMATION 第2巻](https://www.pinkpineapple.co.jp/detail.php?did=2364) | Ⅰ | -
20220930 | [[ショーテン]SLEEPLESS ～A Midsummer Night’s Dream～ The Animation 下巻](http://showten.info/products/anime/stap020.html) | Ⅲ | 包含 CBT(Cock & ball torture, 鸡 & 蛋折磨) 元素。难以接受的请略过。
20220930 | [[魔人 petit]錬精術士コレットのHな搾精物語 第一話 コレットのアトリエ開業！ 爆乳勇者シルヴィアとの出会い](https://www.a1c.jp/~majin/product/collet_01.html) | Ⅱ | 虽然含有Ⅱ类元素，但不够色！
20220930 | [[妄想専科]ＳＵＭＭＥＲ 〜田舎の性活〜　第二話　夏休みの課題とワンピース](https://www.a1c.jp/brandpage/mousousenka/product/summer_02.html) | Ⅰ | -
20220909 | [[King Bee]J<span title="K">〇</span>限界交尾 ～合意挿入でバチバチ肉穴化～ 第2話 チチのチ晴れ](https://www.mediabank.co.jp/product.php?model=KNB-M005) | Ⅰ | 又是女主倒贴和本月的《OVA委員長は催眠アプリを信じてる。》一样卧龙凤雏。
20220902 | [[ばにぃうぉ～か～]OVA 委員長は<span title="催眠">○○</span>アプリを信じてる。](https://www.lune-soft.jp/ova/24829) | Ⅰ | 剧情和上面那部差不多，只是作画更好一些。
20220826 | [[ショーテン]処女はお姉さまに恋してる 三つのきら星 The Animation 下巻](https://showten.info/products/anime/stap018.html) | 待分类 | -
20220826 | [[ピンクパイナップル]搾精病棟 THE ANIMATION 第3巻 ～ヤマグチ編～](https://www.pinkpineapple.co.jp/detail.php?did=2342) | 待分类 | -
20220826 | [[PoRO petit]ツグナヒ ナマイキスポ処女・ナツキ～稚拙な屈辱～](http://www.poro.cc/product/tsugunai_02/petit_tsugunai_02_point.html) | 待分类 | -
20220826 | [[PoRO petit]エロ医師 ナマイキドエロ・怜奈＆綾乃～絡み貪る牝少女～](http://www.poro.cc/product/erodoctor_04/petit_erodoctor_04_point.html) | 待分类 | -
20220826 | [[ピンクパイナップル]鬼作 令和版](https://www.pinkpineapple.co.jp/detail.php?did=2333) | 待分类 | -
20220826 | [[Collaboration Works petit]きつね娘のエッチなお宿 第一話 恩返しで新米女将!?お宿を守るエッチなサービス](https://www.a1c.jp/~collabo/product/kitunemusume_01.html) | 待分类 | -
20220805 | [[メリー・ジェーン]僕は小さな淫魔のしもべ 第1話 家畜生活の始まり](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20220805 | [[ばにぃうぉ～か～]OVA 聖華女学院公認竿おじさん ＃4 金髪爆乳生徒会長ルイーザ・リヒター](https://www.lune-soft.jp/ova/24751) | 待分类 | -
20220805 | [[ばにぃうぉ～か～]OVA 聖華女学院公認竿おじさん ＃3 巴と美桜と、誕生日3Pデート](https://www.lune-soft.jp/ova/24730) | 待分类 | -
20220729 | [[ピンクパイナップル]同級生リメイク THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2331) | 待分类 | -
20220729 | [[GOLD BEAR]聖<span title="奴隷">○○</span>学園2 ～後編～ 剥奪された権力](https://www.mediabank.co.jp/product.php?model=GBR-027) | 待分类 | -
20220729 | [[ピンクパイナップル]搾精病棟 THE ANIMATION 第2巻 ～クロカワ編～](https://www.pinkpineapple.co.jp/detail.php?did=2340) | 待分类 | -
20220729 | [[nur]ハヂ＋ ～晒される羞チ心～](http://nur.a1c.jp/nur_brand/product/hadiplus/index_hadiplus_01.html) | 待分类 | -
20220729 | [[鈴木みら乃 petit]コンビニ<span title="少女">○○</span>Z 第四話 あなた、コンビニマネですよね。本社に万引きがバレていいんですか？](http://www.suzukimirano.com/product/convenie_z_04.html) | 待分类 | -
20220729 | [[妄想専科]ＳＵＭＭＥＲ 〜田舎の性活〜　第一話　日焼けの跡とスクール水着](http://www.a1c.jp/brandpage/mousousenka/product/summer_01.html) | 待分类 | -
20220729 | [[ショーテン]SLEEPLESS ～A Midsummer Night’s Dream～ The Animation 上巻](https://showten.info/products/anime/stap019.html) | 待分类 | -
20220722 | [[Queen Bee]ハーレム・カルト 2 side HAREM\[宇場義行\]](https://www.mediabank.co.jp/product.php?model=QNB-M108) | 待分类 | -
20220708 | [[WHITE BEAR]夏休み明けの彼女は… 前編](https://www.mediabank.co.jp/product.php?model=WBR-112) | 待分类 | -
20220701 | [[ばにぃうぉ～か～]OVA 聖華女学院公認竿おじさん ＃2 陸上少女 加藤美桜](https://www.lune-soft.jp/ova/24705) | 待分类 | -
20220701 | [[ばにぃうぉ～か～]OVA 聖華女学院公認竿おじさん ＃1 黒髪清楚お嬢様 如月巴](https://www.lune-soft.jp/ova/24660) | 待分类 | -
20220624 | [[ショーテン]処女はお姉さまに恋してる 三つのきら星 The Animation 上巻](http://showten.info/products/anime/stap017.html) | 待分类 | -
20220624 | [[PoRO petit]ツグナヒ 高飛車お姫様・瑠璃子～はじまりの痴激～](http://www.poro.cc/product/tsugunai_01/petit_tsugunai_01_point.html) | 待分类 | -
20220624 | [[妄想専科]コスプレチェンジ～ピュア系女子大生の危険な性癖～ 第四話 巨乳女子大生がコスプレ七変化!?誘惑の巨乳戦士は全てお金で解決できちゃう浪花っ娘（なにわっこ）](https://www.a1c.jp/brandpage/mousousenka/product/cosplaychange_04.html) | 待分类 | -
20220624 | [[PoRO petit]エロ医師 清純ドエロ・綾乃～漲る好奇芯～](http://www.poro.cc/product/erodoctor_03/petit_erodoctor_03_point.html) | 待分类 | -
20220624 | [[ピンクパイナップル]＃今までで一番良かったセックス THE ANIMATION 第2巻](https://www.pinkpineapple.co.jp/detail.php?did=2337) | Ⅰ | -
20220624 | [[ピンクパイナップル]＃今までで一番良かったセックス THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2335) | Ⅰ | -
20220624 | [[妄想実現めでぃあ]OVA パンデミック](http://mmdia.net/#widget-page-474) | 待分类 | -
20220624 | [[King Bee]J<span title="K">〇</span>限界交尾 ～合意挿入でバチバチ肉穴化～ 第1話 性活部へようこそ](https://www.mediabank.co.jp/product.php?model=KNB-M004) | 待分类 | -
20220617 | [[Queen Bee]オタクに優しいギャルとか、巨乳の幼なじみとか。 2 [ふみひこ]](https://www.mediabank.co.jp/product.php?model=QNB-M107) | 待分类 | -
20220603 | [[ばにぃうぉ～か～]OVA巨乳女戦士・土下座<span title="催眠">○○</span> ＃2](https://www.lune-soft.jp/ova/24588) | 待分类 | -
20220603 | [[ばにぃうぉ～か～]OVA巨乳女戦士・土下座<span title="催眠">○○</span> ＃1](https://www.lune-soft.jp/ova/24566) | 待分类 | -
20220527 | [[Queen Bee]ハーレム・カルト 1 side HAREM[宇場義行]](https://www.mediabank.co.jp/product.php?model=QNB-M105) | 待分类 | -
20220527 | [[魔人 petit]<span title="援助">○○</span>交配 第六話 真面目な彼女は鬼の姫巫女](https://www.a1c.jp/~majin/product/kouhai_06.html) | 待分类 | -
20220527 | [[あんてきぬすっ]OVA異世界ヤリサー ＃2 百合カップルと人妻 男の良さ教えてやんよw](https://www.lune-soft.jp/ova/24510) | 待分类 | -
20220527 | [[あんてきぬすっ]OVA異世界ヤリサー ＃1 女戦士と新米女冒険者 異世界でもハメちゃうっしょw](https://www.lune-soft.jp/ova/24488) | 待分类 | -
20220527 | [[nur]小さな蕾のその奥に…… ～妖しく齧る爛れた蕾……～](http://nur.a1c.jp/nur_brand/product/tubomi/index_tubomi_04.html) | 制作中(00:40/03:42)
20220527 | [[鈴木みら乃 petit]コンビニ<span title="少女">○○</span>Z　第三話 あなた、ヤンクレママですよね。旦那に万引きがバレていいんですか？](http://www.suzukimirano.com/product/convenie_z_03.html) | 待分类 | -
20220506 | [[メリー・ジェーン]思春期のお勉強 第2話 学ぶより経験がしたいお年頃](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20220428 | [[ショーテン]闇憑村/めるてぃーりみっと The Animation 下巻](http://showten.info/products/anime/stap016.html) | 待分类 | -
20220428 | [[nur]パパ喝ッ！ ～イキ場に漏れる背徳の小水～](http://nur.a1c.jp/nur_brand/product/papakatu/index_papakatu_02.html) | 待分类 | -
20220428 | [[妄想専科]コスプレチェンジ～ピュア系女子大生の危険な性癖～ 第三話 巨乳女子大生がコスプレ七変化!?蠱惑の吸血娘はSMでフタナリな変態プレイがお好き](https://www.a1c.jp/brandpage/mousousenka/product/cosplaychange_03.html) | 待分类 | -
20220428 | [[PoRO petit]エロリーマン 高飛車虐めッ娘・梨々香～堕ちぶれた媚尻～](http://www.poro.cc/product/eroriman_02/petit_eroriman_02_point.html) | 待分类 | -
20220415 | [[Queen Bee]オタクに優しいギャルとか、巨乳の幼なじみとか。 1 [ふみひこ]](https://www.mediabank.co.jp/product.php?model=QNB-M104) | 待分类 | -
20220408 | [[メリー・ジェーン]アネハメ 俺の初恋が実姉なわけがない 第2話 ラブホとお姉ちゃん](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20220401 | [[あんてきぬすっ]OVA 淫行教師の<span title="催眠">○○</span>セイ活指導録 #2 橘弥生編](https://www.lune-soft.jp/ova/24400) | 待分类 | -
20220401 | [[あんてきぬすっ]OVA 淫行教師の<span title="催眠">○○</span>セイ活指導録 #1 藤宮恵編](https://www.lune-soft.jp/ova/24399) | 待分类 | -
20220325 | [[ショーテン]闇憑村/めるてぃーりみっと The Animation 上巻](https://showten.info/products/anime/stap015.html) | 待分类 | -
20220325 | [[PoRO petit]エロ医師 ワイセツチン療・綾乃＆怜奈～ちょろハメ危嬉いっぱつ～](http://www.poro.cc/product/erodoctor_02/petit_erodoctor_02_point.html) | 待分类 | -
20220325 | [[ピンクパイナップル]らぶみー「楓と鈴」THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2329) | 待分类 | -
20220325 | [[魔人 petit]うさみみボウケンタン～セクハラしながら世界を救え～ 第四話 最終決戦！勇者と精霊は結ばれ、想いは受け継がれる](https://www.a1c.jp/~majin/product/usamimi_04.html) | 待分类 | -
20220325 | [[魔人 petit]<span title="援助">○○</span>交配 第五話 内気な彼女は人魚の歌姫](https://www.a1c.jp/~majin/product/kouhai_05.html) | 待分类 | -
20220325 | [[PashminaA]パシュミナフェチ どうしてもイキたい痴女系女子たち編](http://www.pashmina-jp.com/dvd/pashfechi/ikitai/main.html) | 待分类 | -
20220318 | [[Queen Bee]ひみつのきち 2 宵（よい）\[キチロク\]](https://www.mediabank.co.jp/product.php?model=QNB-M103) | 待分类 | -
20220311 | [[メリー・ジェーン]Abandon ‐100ヌキしないと出られない不思議な教室‐ 第1話 闇のSEXゲーム](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20220304 | [[メリー・ジェーン]初めてのヒトヅマ 第4話 ビッチな女子の恋愛相談](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20220304 | [[ばにぃうぉ～か～]OVA <span title="催眠">○○</span>性指導 ＃6](https://www.lune-soft.jp/ova/24207) | 待分类 | -
20220304 | [[ばにぃうぉ～か～]OVA <span title="催眠">○○</span>性指導 ＃5](https://www.lune-soft.jp/ova/24182) | 待分类 | -
20220225 | [[nur]小さな蕾のその奥に…… ～無垢な指先の悪戯～](http://nur.a1c.jp/nur_brand/product/tubomi/index_tubomi_03.html) | 待分类 | -
20220225 | [[鈴木みら乃 petit]コンビニ<span title="少女">○○</span>Z 第二話 あなた、お茶汲み○Lですよね。会社に万引きがバレていいんですか？](http://www.suzukimirano.com/product/convenie_z_02.html) | 待分类 | -
20220225 | [[ピンクパイナップル]キミはやさしく寝取られる THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2317) | 待分类 | -
20220225 | [[PoRO petit]エロリーマン 真苛面目られッ娘・美冬～壊れかけのスマホ～](http://www.poro.cc/product/eroriman_01/petit_eroriman_01_point.html) | 待分类 | -
20220225 | [[ピンクパイナップル]Garden～高嶺家の二輪花～ THE ANIMATION](https://www.pinkpineapple.co.jp/detail.php?did=2315) | 待分类 | -
20220225 | [[PashminaA]パシュミナフェチ SM・調教・お仕置き編 多人数プレイのおまけ付き](http://www.pashmina-jp.com/dvd/pashfechi/sm/main.html) | 待分类 | -
20220218 | [[Queen Bee]サキュバスアプリ ～学園<span title="催眠">○○</span>～ 第4話\[溝口ぜらちん\]](https://www.mediabank.co.jp/product.php?model=QNB-M101) | 待分类 | -
20220211 | [[GOLD BEAR]聖<span title="奴隷">○○</span>学園2 ～前編～ 剥奪された権力](https://www.mediabank.co.jp/product.php?model=GBR-025) | 待分类 | -
20220204 | [[Queen Bee]ひみつのきち 1 暁（あかつき）[キチロク]](https://www.mediabank.co.jp/product.php?model=QNB-M100) | 待分类 | -
20220204 | [[あんてきぬすっ]OVA 茜ハ摘マレ染メラレル ＃2](https://www.lune-soft.jp/ova/24069) | 待分类 | -
20220204 | [[あんてきぬすっ]OVA 茜ハ摘マレ染メラレル ＃1](https://www.lune-soft.jp/ova/24035) | 待分类 | -
20220128 | [[PashminaA]パシュミナフェチ 禁断の姉食い妹食い編](http://www.pashmina-jp.com/dvd/pashfechi/kindan/main.html) | 待分类 | -
20220128 | [[妄想専科]コスプレチェンジ～ピュア系女子大生の危険な性癖～ 第二話 巨乳女子大生がコスプレ七変化!?疑惑の取り調べ室で手錠拘束プレイ](https://www.a1c.jp/brandpage/mousousenka/product/cosplaychange_02.html) | 待分类 | -
20220128 | [[PoRO petit]エロ医師 清純無垢っつり・綾乃～不純診察中ズリ触診～](http://www.poro.cc/product/erodoctor_01/petit_erodoctor_01_point.html) | 待分类 | -
20220128 | [[ピンクパイナップル]やみつきフェロモン THE ANIMATION 第2巻](https://www.pinkpineapple.co.jp/detail.php?did=2307) | 待分类 | -
20220128 | [[ピンクパイナップル]やみつきフェロモン THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2305) | 待分类 | -
20220128 | [[魔人 petit]うさみみボウケンタン～セクハラしながら世界を救え～ 第三話 刺激的な街にご用心！勇者のモテ期と世界の真実](http://www.a1c.jp/~majin/product/usamimi_03.html) | 待分类 | -
20220128 | [[ばにぃうぉ～か～]OVA 巨乳エルフ母娘<span title="催眠">○○</span> ＃2 女王と姫の快楽調教。守護騎士は<span title="恥辱">○○</span>に沈む](https://www.lune-soft.jp/ova/24008) | 待分类 | -
20220128 | [[ばにぃうぉ～か～]OVA 巨乳エルフ母娘<span title="催眠">○○</span> ＃1 エルフの国を蹂躙する男。汚された女王と姫](https://www.lune-soft.jp/ova/23977) | 待分类 | -
20211224 | [[ピンクパイナップル]我が家のリリアナさん＆となりの家のアネットさん THE ANIMATION クライマックス メガ盛り オカズですよ](https://www.pinkpineapple.co.jp/detail.php?did=2254) | 待分类 | -
20211224 | [[PoRO petit]家属～母と姉妹の嬌声～ 足裏熟女・乙葉～指示裏ハマるつちふまず～](http://www.poro.cc/product/kazoku04/petit_kazoku04_point.html) | 待分类 | -
20211224 | [[nur]パパ喝ッ！ ～生イキ濯ぐ恥貝の膜開け～](http://nur.a1c.jp/nur_brand/product/papakatu/index_papakatu_01.html) | 待分类 | -
20211224 | [[鈴木みら乃 petit]コンビニ<span title="少女">○○</span>Z 第一話 あなた、地下アイドルですよね。社長に万引きがバレていいんですか？](http://www.suzukimirano.com/product/convenie_z_01.html) | 待分类 | -
20211224 | [[メリー・ジェーン]アネハメ 俺の初恋が実姉なわけがない 第1話 帰ってきたお姉ちゃん](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20211224 | [[ピンクパイナップル]がーるずらっしゅ THE ANIMATION 第2巻](https://www.pinkpineapple.co.jp/detail.php?did=2311) | 待分类 | -
20211224 | [[ピンクパイナップル]がーるずらっしゅ THE ANIMATION 第1巻](https://www.pinkpineapple.co.jp/detail.php?did=2309) | 待分类 | -
20211217 | [[メリー・ジェーン]思春期のお勉強 第1話 興味津々なお年頃](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20211210 | [[Queen Bee]サキュバスアプリ ～学園<span title="催眠">○○</span>～ 第3話\[溝口ぜらちん\]](https://www.mediabank.co.jp/product.php?model=QNB-M099) | 待分类 | -
20211203 | [[ばにぃうぉ～か～]OVA 千鶴ちゃん開発日記 ＃6](https://www.lune-soft.jp/ova/22675) | 待分类 | -
20211203 | [[ばにぃうぉ～か～]OVA 千鶴ちゃん開発日記 ＃5](https://www.lune-soft.jp/ova/22657) | 待分类 | -
20211126 | [[メリー・ジェーン]初めてのヒトヅマ 第3話 デリバリーセックス](https://mary-jane.biz/mysite1/shouhinlist.html) | 待分类 | -
20211126 | [[ピンクパイナップル]ママホリック ～魅惑のママと甘々カンケイ～ THE ANIMATION 下巻](https://www.pinkpineapple.co.jp/detail.php?did=2301) | 待分类 | -
20211126 | [[ピンクパイナップル]ママホリック ～魅惑のママと甘々カンケイ～ THE ANIMATION 上巻](https://www.pinkpineapple.co.jp/detail.php?did=2299) | 待分类 | -
20211126 | [[妄想専科]身体で解決 百鬼屋探偵事務所 ～百鬼屋 光の妖怪事件簿～ 第四話 妖怪大戦争復讐劇](https://www.a1c.jp/brandpage/mousousenka/product/hyakkiya04.html) | 待分类 | -
20211126 | [[妄想専科]コスプレチェンジ～ピュア系女子大生の危険な性癖～ 第一話 巨乳女子大生がコスプレ七変化!?魅惑の妖怪探偵にムチエロチェンジ](http://www.a1c.jp/brandpage/mousousenka/product/cosplaychange_01.html) | 待分类 | -
20211126 | [[nur]そしてわたしはセンセイに…… ～脇の下のアイツ……～](http://nur.a1c.jp/nur_brand/product/soshisen/index_soshisen_02.html) | 待分类 | -
20211119 | [[ZIZ]対魔忍不知火～淫欲の<span title="奴隷">○○</span>娼婦～](https://www.ziz-entertainment.com/zizd019/index.html) | 待分类 | -
20211119 | [[WHITE BEAR]続・王女＆女騎士Wド下品露出 ～後編～ <span title="恥辱">○○</span>の見世物<span title="奴隷">○○</span>](https://www.mediabank.co.jp/product.php?model=WBR-110) | 待分类 | -
20211105 | [[Queen Bee]ビッチな淫姉さまぁ ＃4[TYPE.90]](https://www.mediabank.co.jp/product.php?model=QNB-M097) | 待分类 | -
20211105 | [[ばにぃうぉ～か～]OVA 千鶴ちゃん開発日記 ＃4](https://www.lune-soft.jp/ova/22639) | 待分类 | -
20211105 | [[ばにぃうぉ～か～]OVA 千鶴ちゃん開発日記 ＃3](https://www.lune-soft.jp/ova/22620) | 待分类 | -
\- | - | - | -
20100319 | [[milky]おっぱいの王者48 第一話](https://web.archive.org/web/20110906065326/http://www.ms-pictures.com/label/milky/products/op48/20100325_op48_01.html) | Ⅰ | -


# 附件

## 01 来源清单
- http://www.getchu.com/all/month_title.html?genre=anime_dvd&gage=adult
- https://www.dlsite.com/maniax/circle/profile/=/maker_id/RG45438.html

## 02 近期活跃的作品<span title="制作, Production">发行</span>和<span title="アニメーション制作, Animation Production">制作</span>官网 HTTPS 协议支持情况

**支持**
- [A1C](https://www.a1c.club)
- [A1C - 2匹目のどぜう](https://www.a1c.jp/~dozeu/index.html)
- [A1C - collaboration works](https://www.a1c.jp/~collabo/index.html)
- [A1C - 妄想専科](https://www.a1c.jp/brandpage/mousousenka/index.html)
- [A1C - 魔人 Petit](https://www.a1c.jp/~majin/top.html)
- [LuneSoft(ルネソフト)](https://www.lune-soft.jp)
- [MaryJane(メリー・ジェーン)](https://mary-jane.biz)
- [MediaBank](https://www.mediabank.co.jp)
- [MediaBank - GOLD BEAR](https://www.mediabank.co.jp/label.php?id=14)
- [MediaBank - HOT BEAR](https://www.mediabank.co.jp/label.php?id=8)
- [MediaBank - King Bee](https://www.mediabank.co.jp/label.php?id=23)
- [MediaBank - Queen Bee](https://www.mediabank.co.jp/label.php?id=22)
- [MediaBank - WHITE BEAR](https://www.mediabank.co.jp/label.php?id=9)
- [PinkPineapple(ピンクパイナップル)](https://www.pinkpineapple.co.jp)
- [showten(ショーテン)](https://showten.info)
- [妄想実現めでぃあ](http://mmdia.net)

**不支持**
- [A1C - PoRO Petit](http://www.poro.cc)
- [A1C - nur(ニュル)](http://nur.a1c.jp/index.html)
- [A1C - 鈴木みら乃](http://www.suzukimirano.com/index.html)
- [PashminaA](http://www.pashmina-jp.com)
- [ZIZ](http://www.ziz-entertainment.com)

<!--
["MaryJane", "LuneSoft", "PinkPineapple", "A1C", "A1C - 魔人 Petit", "A1C - 妄想専科", "A1C - collaboration works", "A1C - 2匹目のどぜう", "MediaBank", "MediaBank - WHITE BEAR", "MediaBank - GOLD BEAR", "MediaBank - HOT BEAR", "MediaBank - Queen Bee", "MediaBank - King Bee", "showten", "妄想実現めでぃあ"].sort()
["A1C - PoRO Petit", "A1C - nur", "A1C - 鈴木みら乃", "PashminaA", "ZIZ"].sort()
-->
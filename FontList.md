中文开源字体清单。

# 清单

字体展示、下载页 | 字重、特殊字型、中文书写法 | 版本、<span title="[字体 Unicode 15.0.0 CJK Unified Ideographs 码位数] / 20992 * 100">CJK 字符涵盖</span> | 协议 | 调用名 | 备注
-- | - | - | - | - | -
[![无虚缺字黑体统一码展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-无虚缺字黑体统一码.png)](https://github.com/notofonts/noto-cjk/releases) | 8(可变、纤、轻、半轻、普、中、粗、重)、1(等宽)、4(SC、TC、HK、JP) | 2.004、20976(99.923%) | [OFL-1.1](https://github.com/googlefonts/noto-cjk/blob/main/Sans/LICENSE) | `Noto Sans [书写法] [字重]`
[![无虚缺字宋体统一码展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-无虚缺字宋体统一码.png)](https://github.com/notofonts/noto-cjk/releases) | 8(可变、极轻、轻、普、中、半粗、粗、重)、0、4(SC、TC、HK、JP) | 2.001、20971(99.899%) | [OFL-1.1](https://github.com/googlefonts/noto-cjk/blob/main/Serif/LICENSE) | `Noto Serif [书写法] [字重]`
[![瀨戶字型展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-瀨戶字型.png)](https://zh.osdn.net/projects/setofont/releases) | 1(普)、0、1(JP) | 6.20、20992(100%) | [OFL-1.1](https://zh.osdn.net/projects/setofont/releases/61995#doc-body-62562) | `SetoFont`
[![霞鹜文楷 GB 展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-霞鹜文楷%20GB.png)](https://github.com/lxgw/LxgwWenkaiGB/releases) | 3(轻、普、粗)、1(等宽)、1(SC) | 0.972、8660(41.253%) | [OFL-1.1](https://github.com/lxgw/LxgwWenkaiGB/blob/main/OFL.txt) | `霞鹜文楷[特殊字型] GB [字重]` `LXGW WenKai [特殊字型] GB [字重]`
[![芫荽展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-芫荽.png)](https://github.com/ButTaiwan/iansui/releases) | 1(普)、0、1(TC) | 0.943、9496(45.236%) | [OFL-1.1](https://github.com/ButTaiwan/iansui/blob/main/OFL.txt) | `Iansui 094`
[![芫茜雅楷展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-芫茜雅楷.png)](https://github.com/ItMarki/jyunsaikaai/releases) | 3(轻、普、粗)、0、1(HK) | 0.2.1、14120(67.263%) | [OFL-1.1](https://github.com/ItMarki/jyunsaikaai#授權許可) | `JyunsaiKaai`
[![霞鹜文楷 TC 展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-霞鹜文楷%20TC.png)](https://github.com/lxgw/LxgwWenkaiTC/releases) | 3(轻、普、粗)、1(等宽)、1(TC) | 0.932、14359(68.573%) | [OFL-1.1](https://github.com/lxgw/LxgwWenkaiTC/blob/main/OFL.txt) | `霞鹜文楷[特殊字型] TC [字重]` `LXGW WenKai [特殊字型] TC [字重]`
[![得意黑展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-得意黑.png)](https://github.com/atelier-anchor/smiley-sans/releases) | 1(普)、1(斜体)、1(SC) | 1.1.1、6766(32.231%) | [OFL-1.1](https://github.com/atelier-anchor/smiley-sans/blob/main/LICENSE) | `Smiley Sans Oblique`
[![噗叽体展示图加载失败，点击此处前往下载页。](IMG/字体清单-字体展示-噗叽体.png)](https://github.com/MY1L/FontGame/releases) | 4(可变、轻、普、极轻)、0、1(SC) | 1.100、119(0.566%) | [綿飴协议](https://github.com/MY1L/icense) | `Ctrl Poop [字重缩写]`

# 附录

**0. 同类仓库**
- [Tamshen/Freecommercialfont](https://github.com/Tamshen/Freecommercialfont)
- [DrXie/OSFCC](https://github.com/DrXie/OSFCC)

**1. 工具推荐**

工具名 | 简介 | 备注
-- | - | -
[FontForge](https://github.com/FontSmaller/fontsmaller.github.io/blob/master/FontSmaller%202.0.rar) | 字体编辑工具。 | Element(元素)-Font Info(字体信息)-Unicode Ranges(统一码涵盖)-CJK Unified Ideographs U+4E00-U+9FFF(中日韩统一表意文字基本区 U+4E00-U+9FFF)
[<span title="字体汉字计数软件">CJK-character-count</span>](https://github.com/NightFurySL2001/CJK-character-count) | 统计字体文件涵盖的汉字字数。
[AssFontSubset](https://github.com/tastysugar/AssFontSubset) | ASS 字幕字体子集化工具。
[FontSmaller](https://github.com/FontSmaller/fontsmaller.github.io/blob/master/FontSmaller%202.0.rar) | 字体子集化工具。
[Dinamo Font Gauntlet](https://fontgauntlet.com) | 可变字体预览网站。

**2. 术语统一**

统一后 | 原命名 | 备注
| - | - | -
纤 | Thin / 100
极轻 | Ultra Light / 200 / W1
超轻 | Extra Light / 200 / W2
轻 | Light / 300 / W3
半轻 | SemiLight / 350 / W4
普 | Regular / Normal / 400
中 | Medium / 500 / W5
半粗 | DemiBold / 600 / W6
粗 | Bold / 700 / W7
超粗 | Extra Bold / 800 / W8
极粗 | Heavy / 900
重 | Black / 900
超重 | Extra Black / 950
意大利体 | Italic | `Italic` 它虽然外观亦是倾斜，但这种斜度脱胎自文艺复兴时期手写体，在笔画中亦有保留手写的痕迹，并非单纯将字符倾斜；
斜体 | Oblique | `Oblique` 就是仅仅将字符倾斜，而没有作出具有手写风格的设计。——[Linkzero Tsang](https://www.zhihu.com/question/23234600/answer/24008305)
等宽 | Mono
细长 | Condensed
矮宽 | Expanded
SC | 中文简体写法 / Simplified Chinese Orthography
TC | 中文繁体写法 / Traditional Chinese Orthography
HK | 中文繁体，香港写法 / 中文繁体，中国香港特别行政区写法 / Traditional Chinese, Hong Kong Orthography / Traditional Chinese, the Hong Kong Special Administrative Region of China Orthography
JP | 日文写法 / Japanese Orthography
OFL-1.1 | SIL Open Font License, 1.1 / SIL 1.1
GPLv3 | GNU General Public License Version 3.0

此部分参考
- https://github.com/MY1L/Ctrl/blob/main/abbr.md
- https://www.zhihu.com/question/23234600/answer/24008305
# 0. 学会更好地提问

请移步至 [ryanhanwu/How-To-Ask-Questions-The-Smart-Way](https://github.com/ryanhanwu/How-To-Ask-Questions-The-Smart-Way) 或 https://acg.is/d/7961-x-gun 。

# 1. 工具

参数 | 描述
| - | -
AO | Any OS, 所有操作系统
AD | Any Desktop, 所有桌面操作系统
AL | Any Linux, 所有 Linux 发行版
W | Windows, 视窗
A | Android, 安卓系统
M | MacOS, 麦金塔系统
LA | Linux-AppImage, Linux 发行版-AppImage 压缩映像
LR | Linux-RPM, Linux 发行版-RPM 软件包管理器
LD | Linux-DEB, Linux 发行版-DEB 软件包管理器

- **开源许可部分许可证缩写参考 https://spdx.org/licenses**

## 1.1 下载器

软件名 | <span title="没错，就是这样！">软件介绍(指针悬浮查看原文)</span> | 开源许可 | 支持系统
| - | - | - | -
[qBittorrent Enhanced Edition](https://github.com/c0re100/qBittorrent-Enhanced-Edition) | <span title="qBittorrent Enhanced, based on qBittorrent 1.Auto Ban Xunlei, QQ, Baidu, Xfplay, DLBT and Offline downloader 2.Auto Ban Unknown Peer from China Option (Default: OFF) 3.Auto Update Public Trackers List (Default: OFF) 4.Auto Ban BitTorrent Media Player Peer Option (Default: OFF) 5.Peer whitelist/blacklist">基于 qBittorrent 的增强版。1.自动屏蔽迅雷、QQ 旋风、百度、影音先锋、DLBT 和单向下载器；2.自动屏蔽来自中国的未知节点(默认禁用)；3.自动更新公共 Tracker 列表(默认禁用)；4.自动屏蔽 BitTorrent 媒体播放器选项(默认禁用)；5.节点节点黑/白名单。</span> | 与原版一致 | AD
[qBittorrent](https://sourceforge.net/projects/qbittorrent/files) | <span title="An advanced and multi-platform BitTorrent client with a nice Qt user interface as well as a Web UI for remote control and an integrated search engine. qBittorrent aims to meet the needs of most users while using as little CPU and memory as possible.">一款高级且支持多平台的 BitTorrent 客户端，采用美观的 Qt 用户界面、拥有用于远控的 Web UI 和一个集成的搜索引擎。qBittorrent 旨在满足大多数用户需求的同时，尽可能降低 CPU 和内存的使用。<span> | 源码部分采用 [GPL-2.0-or-later](https://github.com/qbittorrent/qBittorrent/blob/master/COPYING.GPLv2)、二进制发行版采用 [ GPL-3.0-or-later](https://github.com/qbittorrent/qBittorrent/blob/master/COPYING.GPLv3) | AD
[BiglyBT](https://github.com/BiglySoftware/BiglyBT/releases) | <span title="Feature-filled Bittorrent client based on the Azureus open source project ">基于 Azureus 开源项目、有丰富功能的 Bittorrent 客户端。</span> | [GPL-2.0-or-later](https://github.com/BiglySoftware/BiglyBT/blob/master/LICENSE) | AD
[Transmission](https://transmissionbt.com/download) | <span title="A Fast, Easy and Free Bittorrent Client For macOS, Windows and Linux ">一款迅速、简单且免费的 Bittorrent 客户端。面向 MacOS、Windows 和 Linux。</span> | [不明确](https://github.com/transmission/transmission-releases/blob/master/COPYING) | AD
[LibreTorrent](https://github.com/proninyaroslav/libretorrent/releases) | <span title="Free and Open Source, full-featured torrent client for Android.">面向安卓的免费、开源、全功能的 torrent 客户端。</span> | [GPL-3.0-or-later](https://github.com/proninyaroslav/libretorrent/blob/master/LICENSE.md) | A
[BiglyBT-Android](https://github.com/BiglySoftware/BiglyBT-Android/releases) | <span title="BiglyBT for Android is an ad-free, fully featured open source bittorrent client and remote control optimized for phones, tablets, Chromebooks, and Android TVs.">BiglyBT 安卓版是一款开源、全功能的开源 bittorrent 客户端和桌面版的远控。面向手机、平板、Chromebooks 和安卓电视。</span> | [GPL-2.0-or-later](https://github.com/BiglySoftware/BiglyBT-Android/blob/master/README.md) | A

# 1.2 媒体播放器

软件名 | <span title="没错，就是这样！">软件介绍(指针悬浮查看原文)</span> | 开源许可 | 支持系统
| - | - | - | -
[VLC Media Player](https://www.videolan.org/vlc/#download) | <span title=" VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols. ">VLC 是一款自由、开源的跨平台多媒体播放器及框架，可播放大多数多媒体文件，以及 DVD、音频 CD、VCD 及各类流媒体协议。</span> | [GPL-2.0-only](https://www.videolan.org/legal.html) | AO
[MPC-BE](https://github.com/Aleksoid1978/MPC-BE) | <span title="универсальный проигрыватель аудио и видеофайлов для операционной системы Windows. ">适用于 Windows 操作系统的通用音视频文件播放器。</span> | [GPL-3.0-or-later](https://github.com/Aleksoid1978/MPC-BE#readme) | W

# 2. 资源

## 2.1 站点

此部分数据更新于 2022-11-07

参数 | 描述
| - | -
广 | 站点内含有广告。
隐 | 站点内含有前端可发现的获取浏览记录等隐私行为。
审 | 被部分国家或地区审查屏蔽。
比 | BitTorrent(比特激流)获取资源。
盘 | 通过网盘、HTTP 协议或 FTP 协议获取资源。
在 | 在线欣赏。
盈 | 站点内含有盗版付费后浏览内容。
三 | 包含[三次元\|现实世界]内容。
正 | 正版购买。

- 聚合
  - [Nyaa-Fap](https://sukebei.nyaa.si) - 广审比三
  - [琉璃神社](https://hacg.mom/wp) - 广隐审比
    - [社区](https://hacg.mon/wp/bbs)
	  - [新域名获取](https://acg.gy)
  - [灵梦御所](https://blog.reimu.net) - 广隐审比盘
    - [社区](https://acg.is)
  - [工口学园](https://www.erogakuen.com) - 广比
- 动画
  - [hanime.tv](https://hanime.tv) - 广审在
  - [Hanime1.me](https://hanime1.me) - 广隐审在
  - [Animeidhentai](https://animeidhentai.com) - 广隐审在
  - [Hentaimama](https://hentaimama.io) - 广审在盘
- 游戏
  - [绅士天堂](https://www.aigalgame.com) - 隐比盘
  - [Nutaku](https://www.nutaku.net/zh) - 隐审盘
  - [Erogames](https://erogames.com/zh) - 隐审盘
- 音声 - 不知道有哪些
- 图片
  - 漫画
    - [e-hentai](https://e-hentai.org) - 广审比在
    - [禁漫天堂](https://18comic.vip) - 广隐审在
  	  - [新域名获取](https://jmcomic.bet)
    - [NoyAcg](https://app.noy.asia) - 广隐在
    - [Pixiv-R18 漫画](https://www.pixiv.net/manga?r=1) - 广隐审在
  - 绘图
    - [Pixiv-R18 插画](https://www.pixiv.net/cate_r18.php) - 广隐审在
    - [Gelbooru](https://gelbooru.com) - 广审在
- 轻小说
  - [Pixiv-R18 小说](https://www.pixiv.net/novel/cate_r18.php) - 广隐审在


## 2.2 找回、溯源

- 找动画
  - 有高质量截图
    - https://trace.moe
      - 点 `Browse a file` 上传截图，如果截图是在线的可以在显示 `Image URL` 输入。
      - 点击 `Search` 按钮，然后等待片刻，把网页往下拖一点，就能找到动画名了。
    - https://yandex.com.tr/gorsel
      - 点搜索框旁边的相机图标，这时会出现一个面板，点左侧的 `Dosya seçin` 按钮上传截图；
      - 如果截图是在线的可以在显示 `Görsel'in adresini girin` 的输入框输入，然后点击 `Bul` 按钮。
      - 等待几秒，把网页往下拖一点，找到 `Görselin bulunduğu siteler` 挨个点开看看有没有动画名。
  - 记得大概作品出品年份
    - https://hanime.tv/browse/seasons
      - 在右上角选择年份，等待片刻。
      - 选择作品贩售季度。
        - 第一季度是 `Winter [年份]`。
        - 第二季度是 `Spring [年份]`。
        - 第三季度是 `Summer [年份]`。
        - 第四季度是 `Fall [年份]`。
      - 找到对应作品后点进去。
      - 点击 `Close Ad` 关闭广告，等待片刻。
      - 把网页往下拖一点，找到  `Alternate Titles`，这里有英文、日文和韩文的作品名。
- 找漫画及绘图
  - https://saucenao.com
    - 点击 `选择文件` 按钮后，点击 `get sauce` 上传截图并分析;
    - 如果截图是在线的可以点击 `~advanced options~`，并在 `URL:` 旁的输入框输入，最后点击 `get sauce` 按钮。
    - 等待片刻，后会分析出结果。
  - https://exhentai.org / https://e-hentai.org
    - 点击顶部输入框下方的 `Show File Search` 按钮。
    - 点击 `选择文件` 按钮后，点击 `File Search` 上传截图并分析。
    - 等待片刻，后会分析出结果。